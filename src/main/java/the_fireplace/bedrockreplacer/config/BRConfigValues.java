package the_fireplace.bedrockreplacer.config;

public class BRConfigValues {
	public static final String REPLACEWITH_DEFAULT = "minecraft:stone";
	public static String REPLACEWITH;
	public static final String REPLACEWITH_NAME = "replacewith";
	public static final boolean RISKYBLOCKS_DEFAULT = false;
	public static boolean RISKYBLOCKS;
	public static final String RISKYBLOCKS_NAME = "riskyblocks";
	public static final int[] DIMENSIONS_DEFAULT = new int[]{};
	public static int[] DIMENSIONS;
	public static final String DIMENSIONS_NAME = "disabledindimensions";
}
